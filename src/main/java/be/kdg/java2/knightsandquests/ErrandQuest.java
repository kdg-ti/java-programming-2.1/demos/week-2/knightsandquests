package be.kdg.java2.knightsandquests;

import org.springframework.stereotype.Component;

@Component
public class ErrandQuest implements Quest{
    @Override
    public void embark() {
        System.out.println("Tadaa!");
    }
}
