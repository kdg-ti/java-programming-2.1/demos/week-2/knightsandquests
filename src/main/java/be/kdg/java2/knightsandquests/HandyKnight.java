package be.kdg.java2.knightsandquests;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HandyKnight implements Knight{
    private Quest quest;

    @Autowired
    public HandyKnight(Quest quest) {
        this.quest = quest;
    }

    @Override
    public void embarkOnQuest() {
        quest.embark();
    }
}
