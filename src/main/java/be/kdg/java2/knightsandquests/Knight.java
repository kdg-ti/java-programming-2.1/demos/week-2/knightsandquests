package be.kdg.java2.knightsandquests;

public interface Knight {
    void embarkOnQuest();
}

