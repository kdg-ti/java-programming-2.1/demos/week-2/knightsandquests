package be.kdg.java2.knightsandquests;

public interface Quest {
    void embark();
}
