package be.kdg.java2.knightsandquests;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class KnightsandquestsApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context =
                SpringApplication.run(KnightsandquestsApplication.class, args);
        context.getBean(Knight.class).embarkOnQuest();
        context.close();

    }

}
